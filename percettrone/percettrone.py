import numpy as np

class Percettrone():
    n = 3
    x = np.empty(n)
    w = np.empty(n)
    y = 0.0

    def __init__(self):
        for i in range(self.n):
            self.x[i] = np.random.random()
            self.w[i] = np.random.random()
    
    def step(self):
        self.y = np.sum(self.x * self.w)

    def print(self):
        for i in range(self.n):
            print(f"{self.x[i]} * {self.w[i]}")
        print(f"->{np.sign(self.y)}\n")

def line(x):
    return 0.37 * x + 7.15

def train_set(n):


if __name__ == "__main__":
    p = Percettrone()
    p.print()
    p.step()
    p.print()